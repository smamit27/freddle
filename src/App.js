import "./app.css";
import Gallery from "./components/gallery/Gallery";
import Register from "./components/register/register";
import {
  BrowserRouter as Router,
  Route
} from "react-router-dom";
import Login from "./components/login/login";
import CustomerProfile from "./components/customerprofile/CustomerProfile";



function App() {
  
  return (
    <div className="App">
      <Router>
      <Route exact path="/">
        <Register/>
      </Route>
      <Route exact path="/login">
        <Login/>
      </Route>
      <Route path="/gallery">
        <Gallery />
      </Route>
      <Route path="/customerprofile">
        <CustomerProfile />
      </Route>
      </Router>
    </div>
  );
}

export default App;
