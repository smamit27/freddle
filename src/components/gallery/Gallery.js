import React, { useEffect, useState } from "react";
import ImageSearch from "./ImageSearch";
import { useHistory } from "react-router-dom";


const Gallery = () => {
  const history = useHistory();

  const [image, setImage] = useState([]);
  const [searchTerm, setSearchterm] = useState("");

  useEffect(() => {
    fetch(
      `https://pixabay.com/api/?key=20359785-7a048852591ae1a7a722c70c3&q=${searchTerm}&image_type=photo&pretty=true`
    )
      .then((res) => res.json())
      .then((data) => {
        setImage(data.hits);
      })
      .catch((error) => console.log(error));
    return () => {};
  }, [searchTerm]);
  const ViewProfile= () =>{
    history.push("/customerprofile");
  }
  return (
    <>
    <div class="flex flex-wrap">
      <h1 class="flex-auto text-xl font-semibold">
      </h1>
      <div class="hover-view text-xl pr-10 pt-10 font-semibold text-gray-500" onClick={ViewProfile}>
      View Profile
      </div>
      </div>
    <div className="container max-auto">
    <ImageSearch searchText={(text) => setSearchterm(text) }/>

      <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-3">
        {image.map((img) =>(
          <div className="max-w-sm rounded overflow-hidden shadow-lg" key={img.id}>
            <img
              src={img.webformatURL}
              alt=""
              className="w-full"
            />
            <div className="px-6 py-4">
              <div className="font-bold text-purple-500 text-xl mb-2">
                Photo by {img.user}
              </div>
              <ul>
                <li>
                  <strong>Views: </strong>{img.views}
                </li>
                <li>
                  <strong>Download: </strong>{img.downloads}
                </li>
              </ul>
            </div>
          </div>
        ))}
      </div>
    </div>
    </>
  );
};

export default Gallery;
