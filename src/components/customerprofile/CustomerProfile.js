import React, { useState } from "react";
import "./CustomerProfile.css";
import ls from "local-storage";
import { useHistory } from "react-router-dom";

const CustomerProfile = () => {
  const getcustomerData = ls.get("customer") || [];
  const history = useHistory();

  const [user, UpdateUser] = useState(getcustomerData);
  const [successMessage,setSuccessMessage] = useState(false);
  const inputChange = (e) => {
    const { id, value } = e.target;
    UpdateUser((prevState) => ({
      ...prevState,
      [id]: value,
    }));
  };
  const updateForm = (event) => {
    event.preventDefault();
    const payload = {
      name: user.name,
      phoneNumber: user.phoneNumber,
      email: user.email,
      address: user.address,
    };
    ls.set("customer", payload);
    setSuccessMessage(true)
    setTimeout(() => {
        history.push("/login");

    }, 2000);

  };
  const ViewGallery= () =>{
    history.push("/gallery");
  }
  return (
      <>
    <div class="flex flex-wrap">
      <h1 class="flex-auto text-xl font-semibold">
      </h1>
      <div class="hover-view text-xl pr-10 pt-10 font-semibold text-gray-500" onClick={ViewGallery}>
      View Gallery
      </div>
      </div>
    <div className="flex max-w-sm mx-auto pb-8">
      <div className="flex-auto p-6">
      <h1 className="customer-header text-center pb-10 ">Customer Profile</h1>

        <div className="flex flex-wrap">
          <h1 className="flex-auto text-xl font-semibold">{user.name}</h1>
          <div className="text-xl font-semibold text-gray-500">
            {user.address}
          </div>
        </div>
        <div className="mb-3">
          <label htmlFor="phoneNumber" className="input__label">
            Phone Number{" "}
          </label>
          <input
            id="phoneNumber"
            defaultValue={user.phoneNumber}
            onChange={inputChange}
            name="phoneNumber"
            type="text"
            className="input__field"
          />
        </div>
        <div className="mb-3 ">
          <label htmlFor="email-address" className="input__label">
            Email
          </label>
          <input
            id="email"
            value={user.email}
            onChange={inputChange}
            name="email"
            type="email"
            className="input__field"
          />
        </div>
        <div className="text-center">
          <button
            type="submit"
            onClick={updateForm}
            className="btn btn--primary btn--hoverable"
          >
            Update
          </button>
          <h1 className=" text-center p-5  text-green-500">
        {successMessage ? 'Customer Updated Successfully' : ""}
        </h1>
        </div>
      </div>
    </div>
    </>
  );
};

export default CustomerProfile;
