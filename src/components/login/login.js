import React, {  useState } from "react";
import ls from "local-storage";
import { useHistory } from "react-router-dom";
import './login.css';
const Login = () => {
  const history = useHistory();
  const [phone, setPhone] = useState({
    loginPhoneNumber: "",
  });
  const [errorMessage,setErrorMessage] = useState(false);
  const inputChange = (e) => {
    const { id, value } = e.target;
    setPhone((prevState) => ({
      ...prevState,
      [id]: value,
    }));
  };
  const submitLoginForm = (event) => {
    event.preventDefault();
    const getcustomerData = ls.get("customer") || [];
    if (phone.loginPhoneNumber === getcustomerData.phoneNumber) {
      history.push("/gallery");
    }
    else {
      setErrorMessage(true)
    }
  };

  return (
    <>
    <h1 className="login text-center p-5 pt-10 md:pt-16">Log in</h1>
      <section id="introductions" className="max-w-sm mx-auto pb-24">
        <div className="mb-3 input">
          <label htmlFor="loginPhoneNumber" className="input__label">
            Phone Number{" "}
          </label>
          <input
            id="loginPhoneNumber"
            value={phone.loginPhoneNumber}
            onChange={inputChange}
            name="loginPhoneNumber"
            type="text"
            className="input__field"
          />
        </div>
        <div className="text-center">
          <button
            type="submit"
            onClick={submitLoginForm}
            className="btn btn--primary btn--hoverable"
          >
            Log in 
          </button>
        </div>
        <h1 className=" text-center p-5  text-red-500">
        {errorMessage ? 'Please enter correct phone number' : ""}
        </h1>

      </section>

    </>
  );
};

export default Login;
