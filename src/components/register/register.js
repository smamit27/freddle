import React, { useState } from "react";
import "./register.css";
import ls from "local-storage";
import { useHistory } from "react-router-dom";


function Register() {
  const history = useHistory();
  const [user, setUser] = useState({
    name: "",
    phoneNumber: "",
    email: "",
    address: "",
  });
  const submitForm = (event) => {
    event.preventDefault();
    const payload = {
      name: user.name,
      phoneNumber: user.phoneNumber,
      email: user.email,
      address: user.address,
    };
    ls.set("customer", payload);
    history.push("/login");

  };
  const inputChange = (e) => {
    const { id, value } = e.target;
    setUser((prevState) => ({
      ...prevState,
      [id]: value,
    }));
  };
  return (
    <div>
      <div className="main">
        <div className="register__sub-navigation">
          <div className="flex icons">
            <div className="w-full introductions">
              <a
                href="/register/account"
                className="router-link-exact-active router-link-active active"
              >
                <div className="icon"></div>
                <span>Registration </span>
              </a>
            </div>
          </div>
        </div>
      </div>
      <section id="introductions" className="max-w-sm mx-auto pb-24">
          <div className="mb-3 input">
            <label htmlFor="name" className="input__label">
              Name
            </label>
            <input
              id="name"
              value={user.name}
              name="name"
              onChange={inputChange}
              type="text"
              className="input__field"
            />
          </div>
          <div className="mb-3 input">
            <label htmlFor="phone-number" className="input__label">
              Phone Number{" "}
            </label>
            <input
              id="phoneNumber"
              value={user.phoneNumber}
              onChange={inputChange}
              name="phone-number"
              type="text"
              className="input__field"
            />
          </div>
          <div className="clear--sm"></div>
          <div className="input">
            <label htmlFor="email-address" className="input__label">
              Email
            </label>
            <input
              id="email"
              value={user.email}
              onChange={inputChange}
              name="email"
              type="email"
              className="input__field"
            />
          </div>
          <div>
            <div className="input">
              <label htmlFor="address" className="input__label">
                Address
              </label>
              <input
                value={user.address}
                onChange={inputChange}
                id="address"
                name="address"
                type="text"
                className="input__field"
              />
            </div>
            <div className="clear--sm"></div>
            <div className="text-center">
              <button
                type="submit"
                onClick={submitForm}
                className="btn btn--primary btn--hoverable"
              >
                Submit
              </button>
            </div>
          </div>
      </section>
    </div>
  );
}

export default Register;
